﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RectangleSpaceShape : ISpaceShape 
{
	public List<Leaf> GenerateCrown(List<Leaf> Leaves, int LeafCount)
	{
		// Variable declarations
		Rectangle crown;
		Vector2 Position = Vector2.zero;
		Generator GenController = Generator.Instance;

		// Create a new crown and declare its list of leaves
		crown = new Rectangle ((int)Position.x - GenController.TreeWidth / 2, (int)Position.y + GenController.TrunkHeight, GenController.TreeWidth, GenController.TreeHeight);
		Leaves = new List<Leaf> ();

		// Randomly place leaves within The crown
		for (int i = 0; i < LeafCount; i++) {
			Vector2 location = new Vector2 (Random.Range (crown.Left (), crown.Right () + 1), Random.Range (crown.Top (), crown.Bottom () + 1));
			Leaf leaf = new Leaf (location, Generator.Instance.LeafDebug);
			Leaves.Add (leaf);
		}
		// Return the list of leaves
		return Leaves;
	}


}
