﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CubeSpaceShape : ISpaceShape 
{
	public List<Leaf> GenerateCrown(List<Leaf> Leaves, int LeafCount)
	{
		// Variable declarations
		Cube crown;
		Vector3 Position = Vector3.zero;
		Generator GenController = Generator.Instance;

		// Create a new crown and declare its list of leaves
		crown = new Cube ((int)Position.x - GenController.TreeWidth / 2, (int)Position.y + GenController.TrunkHeight, (int)Position.z - GenController.TreeDepth / 2, GenController.TreeWidth, GenController.TreeHeight, GenController.TreeDepth);
		Leaves = new List<Leaf> ();

		// Randomly place leaves within The crown
		for (int i = 0; i < LeafCount; i++) {
			Vector3 location = new Vector3 (Random.Range (crown.Left (), crown.Right () + 1), Random.Range (crown.Top (), crown.Bottom () + 1), Random.Range (crown.Front (), crown.Back () + 1));
			Leaf leaf = new Leaf (location, Generator.Instance.LeafDebug);
			Leaves.Add (leaf);
		}
		// Return the list of leaves
		return Leaves;
	}
}