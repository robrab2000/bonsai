﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SphereSpaceShape : ISpaceShape
{
    public List<Leaf> GenerateCrown(List<Leaf> Leaves, int LeafCount)
    {
        // Fetch the width of the tree to use as a radius
        int r = Generator.Instance.TreeWidth / 2;
        // Iterate through the number of leaves to be created
        for (int i = 0; i < Generator.Instance.LeafCount; i++)
        {
            int x;
            int y;
            int z;

            // Keep cycling through uniformly random variables until values that fall within the sphere are found
            do
            {
                x = Random.Range(-r, r);
                y = Random.Range(-r, r);
                z = Random.Range(-r, r);
            } while (Mathf.Pow(x, 2f) + Mathf.Pow(y, 2f) + Mathf.Pow(z, 2f) > Mathf.Pow(r, 2f));
            
            // Create a leaf at this location and add it to the list
            Vector3 location = new Vector3(x, y + Generator.Instance.TrunkHeight + r, z);
            Leaf leaf = new Leaf(location, Generator.Instance.LeafDebug);
            Leaves.Add(leaf);
        }
        // Return the list of leaves
        return Leaves;
    }
}

