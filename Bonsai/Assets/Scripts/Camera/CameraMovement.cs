﻿using UnityEngine;
using System.Collections;

public class CameraMovement : MonoBehaviour {

    public GameObject Leader;
    public Vector3 ViewOffSet;
    public float speed;
    
	void Start()
    {
		// If there is no leader object set, locate the leader object
        if (Leader == null)
        {
            Leader = GameObject.Find("CamLeader");
        }
    }

    void FixedUpdate()
    {
		// Make sure the camera always faces the tree, aiming at the top of the trunk
		transform.LookAt(Vector3.zero + new Vector3(0f, (float)Generator.Instance.TrunkHeight, 0f));
        this.transform.position = Vector3.Lerp(this.transform.position, Leader.transform.position, Time.deltaTime * speed);
    }
	
}
