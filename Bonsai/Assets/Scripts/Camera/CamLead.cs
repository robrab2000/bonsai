﻿using UnityEngine;
using System.Collections;

public class CamLead : MonoBehaviour {
    
    public float speed = 1f;
    public float distance;

    // Use this for initialization
    void Start () {

	}

    // Update is called once per frame
    void Update()
    {
        //if (target != null)
        //{
            if (distance < 5f)
            {
                distance = Vector3.Distance(this.transform.position, Vector3.zero);
            }
            transform.LookAt(Vector3.zero);
            transform.Translate(Vector3.right * (Time.deltaTime * Input.GetAxis("Horizontal") * speed));
            if (Vector3.Distance(this.transform.position, Vector3.zero) > distance)
            {
                this.transform.Translate(Vector3.forward * speed);
            }
        //}
    }
}
