﻿using UnityEngine;
using System.Collections;

public class Rectangle
{
	public int xPos { get; set; }
	public int yPos { get; set; }
	public int width { get; set; }
	public int height { get; set; }

	public Rectangle (
		int Xpos,
		int Ypos,
		int Width,
		int Height
	)
	{
		xPos = Xpos;
		yPos = Ypos;
		width = Width;
		height = Height;
	}

	public int Left() {
		return xPos;
	}
	public int Right() {
		return xPos + width;
	}
	public int Top() {
		return yPos + height;
	}
	public int Bottom() {
		return yPos;
	}

}
