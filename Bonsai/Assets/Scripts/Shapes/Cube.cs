﻿using UnityEngine;
using System.Collections;

public class Cube
{
	public int xPos { get; set; }
	public int yPos { get; set; }
	public int zPos { get; set; }
	public int width { get; set; }
	public int height { get; set; }
	public int depth { get; set; }

	public Cube (
		int Xpos,
		int Ypos,
		int Zpos,
		int Width,
		int Height,
		int Depth
	)
	{
		xPos = Xpos;
		yPos = Ypos;
		zPos = Zpos;
		width = Width;
		height = Height;
		depth = Depth;
	}

	public int Left() {
		return xPos;
	}
	public int Right() {
		return xPos + width;
	}
	public int Front() {
		return zPos;
	}
	public int Back() {
		return zPos + depth;
	}
	public int Top() {
		return yPos + height;
	}
	public int Bottom() {
		return yPos;
	}

}