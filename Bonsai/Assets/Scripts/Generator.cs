﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum SpaceShapes
{
	Rectangle,
	Cube,
	Sphere,
	Import
}

public class Generator : MonoBehaviour
{

	//#region Singleton Check
	private static Generator generatorInstance = null;

	public static Generator Instance {
		get { return generatorInstance; }
	}

	void Awake ()
	{
		if (generatorInstance != null && generatorInstance != this) {
			Destroy (this.gameObject);
			return;
		} else {
			generatorInstance = this;
		}
		DontDestroyOnLoad (this.gameObject);
	}

	//#endregion


	// Variable declarations
	public SpaceTree MyTree;
	public SpaceShapes SelectedSpace = SpaceShapes.Rectangle;
	public int LeafCount = 400;
	public int TreeWidth = 150;
	public int TreeHeight = 120;
	public int TreeDepth = 150;
	public int TrunkHeight = 40;
	public int MinDistance = 2;
	public int MaxDistance = 15;
	public int BranchLength = 2;
	[Range (0.0f, 100.0f)]
	public float FoliageDepth = 20;
	[Range (0.0f, 100.0f)]
	public float FoliageDensity = 50;
    public float FoliageVariability = 10f;
    public float ExpansionRate = 1.1f;
	public float ExpansionVariability = 1f;
	public float MaxExpansion = 3f;
	public float TrunkMaxModifier = 3f;
    public bool LeafDebug = false;
    public Material BarkMaterial;
    public Material FoliageMaterial;
	public Mesh[] FoliageModels;


    // Use this for initialization
    void Start ()
	{
		// Define the new tree
		MyTree = new SpaceTree (Vector3.zero);

	}
	
	// Update is called once per frame
	void Update ()
	{
		// Grow the new tree
		MyTree.Grow ();
	}
}
