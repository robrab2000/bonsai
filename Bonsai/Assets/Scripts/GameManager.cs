﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameManager : MonoBehaviour {

	//#region Singleton Check
	private static GameManager gameManagerInstance = null;

	public static GameManager Instance {
		get { return gameManagerInstance; }
	}

	void Awake ()
	{
		if (gameManagerInstance != null && gameManagerInstance != this) {
			Destroy (this.gameObject);
			return;
		} else {
			gameManagerInstance = this;
		}
		DontDestroyOnLoad (this.gameObject);
	}

	//#endregion

	// Variable declarations
	public GameObject BranchPrefab, LeafPrefab;
	public List<GameObject> Branches = new List<GameObject>();
	public List<GameObject> Leaves = new List<GameObject>();
	public List<GameObject> Foliage = new List<GameObject>();
	public int branchCounter = 0, leafCounter = 0;
	public GameObject TheTree, LeafCloud;
    public bool RotateTree = false;



	//! Function to create a new tree if none exists
	public void CreateTree()
	{
		if (TheTree == null) {
			TheTree = new GameObject ();
			TheTree.name = "TheTree";
		}
	}

	//! Function to create a new leaf cloud if none exists
	public void CreateLeafCloud()
	{
		if (LeafCloud == null) {
			LeafCloud = new GameObject ();
			LeafCloud.name = "LeafCloud";
		}
	}

	//! Function for executing controls
	void SystemControls() {
		// Exit Game
		if (Input.GetKeyDown (KeyCode.Escape)) {
			Application.Quit ();
		}
	}


	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		SystemControls ();
	}
}
