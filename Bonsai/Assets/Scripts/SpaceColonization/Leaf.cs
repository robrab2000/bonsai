﻿using UnityEngine;
using System.Collections;

public class Leaf
{
	public Vector3 Position { get; set; }
	public Branch ClosestBranch { get; set; }
	public GameObject LeafObject { get; set; }
	public Leaf (Vector3 position, bool visualize)
	{
		// Define leaf position
		Position = position;
		// Visualize the leaf if requested
		if (visualize) {
			LeafObject = UnityEngine.MonoBehaviour.Instantiate (GameManager.Instance.LeafPrefab, position, Quaternion.identity) as GameObject;
			GameManager.Instance.leafCounter++;
			LeafObject.transform.parent = GameManager.Instance.LeafCloud.transform;
			LeafObject.name = ("Leaf " + GameManager.Instance.branchCounter);
			GameManager.Instance.Leaves.Add (LeafObject);
		}
	}
}