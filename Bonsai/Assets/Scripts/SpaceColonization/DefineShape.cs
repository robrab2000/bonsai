﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public interface ISpaceShape
{
	// Every spaceshape must have a GenerateCrown function which takes in a List of leaves and a leaf count, and returns a list of leaves
	List<Leaf> GenerateCrown (List<Leaf> Leaves, int LeafCount);
}

public class DefineShape 
{
	//! Function to generate a shape and populate it with a leaf cloud
	public List<Leaf> GenerateShape(SpaceShapes newShape, int LeafCount) {
		// Declare an empty list of leaves to populate
		List<Leaf> Leaves = new List<Leaf> ();

		// Switch case to generate all the various shapes
		switch (Generator.Instance.SelectedSpace) {
		case SpaceShapes.Rectangle:
			RectangleSpaceShape NewRectangleShape = new RectangleSpaceShape ();
			Leaves = NewRectangleShape.GenerateCrown (Leaves, LeafCount);
			break;
		case SpaceShapes.Cube:
			CubeSpaceShape NewCubeShape = new CubeSpaceShape ();
			Leaves = NewCubeShape.GenerateCrown (Leaves, LeafCount);
			break;
		case SpaceShapes.Sphere:
                SphereSpaceShape NewSphereShape = new SphereSpaceShape();
                Leaves = NewSphereShape.GenerateCrown(Leaves, LeafCount);
                break;
		case SpaceShapes.Import:
			break;
		}
		// Return the generated list of leaves
		return Leaves;
	}
}
