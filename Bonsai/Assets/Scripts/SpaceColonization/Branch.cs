﻿using UnityEngine;
using System.Collections;

public class Branch
{
	// Variable declarations
	public Branch Parent { get; private set; }
	public Vector3 GrowDirection { get; set; }
	public Vector3 OriginalGrowDirection { get; set; }
	public int GrowCount { get; set; }
	public Vector3 Position { get; private set; }
	public GameObject BranchObject { get; set; }
	public float BranchMaxModifier { get; set; }
	public bool FoliageBranch { get; set; }
	public float FoliageModifier { get; set; }
	private bool foliageSet = false;
	private MonoBehaviour coroutineMonoBehavior;
	private IEnumerator foliageExpandCoroutine;

	//! Branch Constructor
	public Branch (Branch parent, Vector3 position, Vector3 growDirection)
	{
		Parent = parent;
		Position = position;
		GrowDirection = growDirection;
		OriginalGrowDirection = growDirection;
		Quaternion newDir = new Quaternion (growDirection.x, growDirection.y, 0, 0);
		BranchObject = UnityEngine.MonoBehaviour.Instantiate (GameManager.Instance.BranchPrefab, position, newDir) as GameObject;
		GameManager.Instance.branchCounter++;
		BranchObject.transform.parent = GameManager.Instance.TheTree.transform;
		BranchObject.name = ("Branch " + GameManager.Instance.branchCounter);
        // Set the material to the bark material
        BranchObject.GetComponent<Renderer>().sharedMaterial = Generator.Instance.BarkMaterial;
		GameManager.Instance.Branches.Add (BranchObject);
		Expand ();
		// Creates a new monobehaviour user to trigger the coroutine
		coroutineMonoBehavior = BranchObject.AddComponent<MonoBehaviour>();
	}

	//! Function to expand the branch
	public void Expand()
	{
		// Modifier to ensure that trunk is thicker at the bottom
        float trunkMod = BranchObject.transform.position.y - Generator.Instance.TrunkHeight;
        if (trunkMod > 0f) { trunkMod = 0f; }
        else { trunkMod *= -0.1f; }

        // Create a foliageModifier variable, just in case we need it
        float foliageModifier = 0f;

		// Foliage Modifications
		if (FoliageBranch && !foliageSet) {
			// Change the mesh
			BranchObject.GetComponent<MeshFilter>().mesh = Generator.Instance.FoliageModels[Random.Range(0,Generator.Instance.FoliageModels.Length)];
            // Change material to the foliage material
            BranchObject.GetComponent<Renderer>().sharedMaterial = Generator.Instance.FoliageMaterial;
			// Modify tree foliage texture color
			BranchObject.GetComponent<Renderer>().sharedMaterial.color = new Color(1f, Random.Range(0.6f,1f),1f,Random.Range(0.6f,1f));
            // Assign the foliageModifier
			foliageModifier = FoliageModifier;
			// Offset texture
            BranchObject.GetComponent<Renderer>().material.mainTextureOffset = new Vector2(Random.Range(0f, 10f), Random.Range(0f, 10f));
			// Scale Texture
            BranchObject.GetComponent<Renderer>().material.mainTextureScale = new Vector2(Random.Range(1f, 5f), Random.Range(1f, 3f));
			//Indicate that foliage has been set
			foliageSet = true;
			// Rename object to indicate that it is foliage
			BranchObject.name = "Foliage";
			// Add this object to the list of foliage
			GameManager.Instance.Foliage.Add(BranchObject);

			// Assign foliage expansion coroutine
			foliageExpandCoroutine = FoliageExpand(0.2f, foliageModifier);
			// Trigger the coroutine to begin
			coroutineMonoBehavior.StartCoroutine(foliageExpandCoroutine);
        }

		// Perform actual expansion
		else if (BranchObject.transform.localScale.x < Generator.Instance.MaxExpansion + BranchMaxModifier + trunkMod + foliageModifier) {
			BranchObject.transform.localScale *= Generator.Instance.ExpansionRate + (foliageModifier * 0.2f);
			BranchObject.transform.rotation = Random.rotation;
			if (Parent != null) {
				Parent.Expand ();
			}
		}
	}

	public IEnumerator FoliageExpand(float expansionDelay, float foliageModifier){
		// Perform actual expansion
		while (BranchObject.transform.localScale.x < Generator.Instance.MaxExpansion + BranchMaxModifier + foliageModifier) {
			BranchObject.transform.localScale *= Generator.Instance.ExpansionRate + (foliageModifier * 0.02f);
			BranchObject.transform.rotation = Random.rotation;
			yield return new WaitForSeconds(expansionDelay);
		}
		// Trigger the coroutine to begin
		coroutineMonoBehavior.StopCoroutine(foliageExpandCoroutine);
	}

	//! Function to reset the branch
	public void Reset ()
	{
		GrowCount = 0;
		GrowDirection = OriginalGrowDirection;
	}
}