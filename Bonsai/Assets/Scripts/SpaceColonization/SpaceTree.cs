﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpaceTree 
{
	// Variable declarations
	bool DoneGrowing = false;

	Vector3 Position = Vector3.zero;

	Branch Root;
	List<Leaf> Leaves;
	Dictionary<Vector3, Branch> Branches;

	//! Function to initialize a new tree
	public SpaceTree (Vector3 position)
	{
		GameManager.Instance.CreateTree ();
		GameManager.Instance.CreateLeafCloud ();
		GenerateCrown ();
		GenerateTrunk ();
	}

	//! Declare a shape generator to generate the leaf space
	private void GenerateCrown ()
	{
		DefineShape ShapeGenerator = new DefineShape();
		Leaves = ShapeGenerator.GenerateShape (Generator.Instance.SelectedSpace, Generator.Instance.LeafCount);
	}

	//! Generate a trunk for the tree
	private void GenerateTrunk ()
	{
		// Define a data structure to hold a list of all the branches
		Branches = new Dictionary<Vector3, Branch> ();

		// Define the Root branch as a new branch, add it to the list of branches
		Root = new Branch (null, Position, new Vector3 (0, 1));
		Branches.Add (Root.Position, Root);

		// Set the current branch by creating a new branch that uses the root as its parent and add it to the list of branches
		Branch current = new Branch (Root, new Vector3 (Position.x, Position.y + Generator.Instance.BranchLength), new Vector3 (0, 1));
		Branches.Add (current.Position, current);
        
        // Keep growing trunk upwards until we reach a leaf       
        while ((Root.Position - current.Position).magnitude/*.Length ()*/ < Generator.Instance.TrunkHeight) {
			Branch trunk = new Branch (current, new Vector3 (current.Position.x, current.Position.y + Generator.Instance.BranchLength), new Vector3 (0, 1));
			Branches.Add (trunk.Position, trunk);
			current = trunk;
			current.BranchMaxModifier = Random.Range (Generator.Instance.TrunkMaxModifier - 3.5f, Generator.Instance.TrunkMaxModifier);
			current.Expand ();
		}         
	}

	//! Function to grow the actual tree
    public void Grow()
    {
        if (DoneGrowing)
        {
			// Insert any actions to take once the growth is complete
        }
		// If no leaves left, we are done
		if (Leaves.Count == 0) { 
			DoneGrowing = true; 
			return; 
		}

		// Process the leaves
		for (int i = 0; i < Leaves.Count; i++) {
			bool leafRemoved = false;

			Leaves [i].ClosestBranch = null;
			Vector3 direction = Vector3.zero;

			// Find the nearest branch for this leaf
			foreach (Branch b in Branches.Values) {
				direction = Leaves [i].Position - b.Position;                       //direction to branch from leaf
				float distance = (float)Mathf.Round (direction.magnitude);            //distance to branch from leaf
				direction.Normalize ();

				if (distance <= Generator.Instance.MinDistance) {            //Min leaf distance reached, we remove it
					Leaves.Remove (Leaves [i]);                        
					i--;
					leafRemoved = true;
					break;
				} else if (distance <= Generator.Instance.MaxDistance) {       //branch in range, determine if it is the nearest
					if (Leaves [i].ClosestBranch == null)
						Leaves [i].ClosestBranch = b;
					else if ((Leaves [i].Position - Leaves [i].ClosestBranch.Position).magnitude/*Length ()*/ > distance)
						Leaves [i].ClosestBranch = b;
				}
			}

			// If the leaf was removed, skip
			if (!leafRemoved) {
				// Set the grow parameters on all the closest branches that are in range
				if (Leaves [i].ClosestBranch != null) {
					Vector3 dir = Leaves [i].Position - Leaves [i].ClosestBranch.Position;
					dir.Normalize ();
					Leaves [i].ClosestBranch.GrowDirection += dir;       //add to grow direction of branch
					Leaves [i].ClosestBranch.BranchObject.transform.rotation = new Quaternion(Leaves [i].ClosestBranch.GrowDirection.x, Leaves [i].ClosestBranch.GrowDirection.y, 0, 0);
					Leaves [i].ClosestBranch.GrowCount++;
				}
			}
		}

		// Generate the new branches
		HashSet<Branch> newBranches = new HashSet<Branch> ();
		foreach (Branch b in Branches.Values) {
			if (b.GrowCount > 0) {    //if at least one leaf is affecting the branch
				Vector3 avgDirection = b.GrowDirection / b.GrowCount;
				avgDirection.Normalize ();

				Branch newBranch = new Branch (b, b.Position + avgDirection * Generator.Instance.BranchLength, avgDirection);

                //if (newBranch.Position)

				newBranches.Add (newBranch);
				b.Reset ();
			}
		}

		// Add the new branches to the tree
		bool BranchAdded = false;
		foreach (Branch b in newBranches) {
			// Check if branch already exists.  These cases seem to happen when leaf is in specific areas
			Branch existing;
			if (!Branches.TryGetValue (b.Position, out existing)) {
                // Add branch to the list
                Branches.Add (b.Position, b);
                // Set the max modifier
				b.BranchMaxModifier = Random.Range (0, Generator.Instance.ExpansionVariability);
                // Perform Foliage Check
                CheckForFoliageZone(b);
                // Set is as a branch added
                BranchAdded = true;
			}
		}

		// If no branches were added - we are done
		// This handles issues where leaves equal out each other, making branches grow without ever reaching the leaf
		if (!BranchAdded)
			DoneGrowing = true;

		//Debug.Log ("Tree is effing DONE!");
	}

    // Funtion to check if the branch falls within the foliage zone
    void CheckForFoliageZone(Branch b)
    {
        // Make a local copy of the vector so we don't have to keep retrieving it for each axis
        Vector3 bPos = b.Position;
        // Switch case for the various shapes
        switch (Generator.Instance.SelectedSpace)
        {
            case SpaceShapes.Rectangle:
            case SpaceShapes.Cube:
                // Calculate X threshold
			float xThresh = (Generator.Instance.TreeWidth / 2) * ((Generator.Instance.FoliageDepth * 0.002f) );//* 0.01f);
                // Calculate Z threshold
			float zThresh = (Generator.Instance.TreeDepth / 2) * ((Generator.Instance.FoliageDepth * 0.002f));// * 0.01f);
                // Calculate Y threshold
			float yThresh = (Generator.Instance.TreeHeight / 2) * ((Generator.Instance.FoliageDepth * 0.002f));// * 0.01f);

                // Check X axis to see if they fall within the foliage zone
                if (bPos.x > (Generator.Instance.TreeWidth / 2) - xThresh || bPos.x < ((Generator.Instance.TreeWidth / 2) * -1) + xThresh)
                {
                    CheckDensity(b);
                    break;
                }
                // Check Z axis to see if they fall within the foliage zone
                if (bPos.z > (Generator.Instance.TreeDepth / 2) - zThresh || bPos.z < ((Generator.Instance.TreeDepth / 2) * -1) + zThresh)
                {
                    CheckDensity(b);
                    break;
                }
                // Check Y axis to see if they fall within the foliage zone
                if ((bPos.y - Generator.Instance.TrunkHeight) > (Generator.Instance.TreeHeight / 2) - yThresh || bPos.y < ((Generator.Instance.TreeHeight / 2) * -1) + yThresh)
                {
                    CheckDensity(b);
                    break;
                }
                break;
            case SpaceShapes.Sphere:
                //Check radius here
                break;
            case SpaceShapes.Import:
                break;
        }
    }

    // Foliage Density Randomizer
    void CheckDensity(Branch b)
    {
        // Check that branch is not already set as foliage
        if (!b.FoliageBranch)
        {
            // Random roll against desired density to determine if it will be foliage
			if (Random.Range(0, 100) > (Generator.Instance.FoliageDensity * 0.2f))
            {
                // If it is outside the density envelope, then it is not foliage
                b.FoliageBranch = false;
                return;
            }
            // Assign a random foliage variability
			b.FoliageModifier = Random.Range((Generator.Instance.FoliageVariability * 0.25f), Generator.Instance.FoliageVariability);
            // If it is within the density envelope then it will be foliage
            b.FoliageBranch = true;
        }
    }
}
